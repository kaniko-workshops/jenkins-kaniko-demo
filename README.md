# Skaffold Kaniko Workshop

## Useful Links

- <https://skaffold.dev/>
- <https://github.com/GoogleContainerTools/kaniko>
- <https://skaffold.dev/docs/pipeline-stages/builders/#in-cluster-build>
- <https://skaffold.dev/docs/references/yaml/?version=v2beta17>
- <https://skaffold.dev/docs/references/cli/#skaffold-deploy>
- <https://skaffold.dev/docs/references/cli/#skaffold-build>
- <https://github.com/GoogleContainerTools/kaniko#pushing-to-different-registries>
- <https://skaffold.dev/docs/pipeline-stages/>

## Prerequisites

- Relatively good Kubernetes knowledge
- Good bash / linux skills
- Access to a privat docker registry account (docker registry, Quay, Harbor, On-prem, etc.)
- **kubectl** configured and connected to a Kubernetes / Minikube cluster

## Preparing your local environment and your Kubernetes (or Minikube) cluster

- First, download and install **Skaffold** by following the instructions corresponding to your Operating System: <https://skaffold.dev/docs/install/>

- Edit the [/skaffold.yaml](skaffold.yaml) file accordingly

- Create a namespace called **ci-cd** (This is just an example name, but it might be different in your case), but p.
  Running this command is only needed once, in the beginning

```bash
kubectl create namespace ci-cd
```

- Copy the [secrets/config.example.json](/secrets/config.exammple.json) file to **secrets/config.json**

```bash
cp secrets/config.exammple.json secrets/config.json
```

- Open the **secrets/config.json** file in your favorite text editor, and replace the **REGISTRY_HOST** and **AUTHORIZATION_HEADER_BASE64** string with the appropriate values (As an example, for **REGISTRY_HOST** we will use **<https://index.docker.io/v1/>** and **AUTHORIZATION_HEADER_BASE64** needs to be the **base64-encoded value** of the following string: **\<your-docker-registry-username\>:\<your-docker-registry-password\>**. Please update the actual values accordingly.

  **NOTE** Please note that normally, you **do not have to** use an URL, but the registry hostname for **REGISTRY_HOST**, but since we are using docker hub, we have to. For mmore details related to this, please see the note found at <https://github.com/GoogleContainerTools/kaniko#pushing-to-docker-hub>

- Within the **ci-cd** namespace create a secret. This secret will be used internally by **Kanikon** to push the images to the given docker repository. Please note that for our demonstrative purposes we will be using the following docker repository: **docker.io/apsro07/sample-app**

```bash
kubectl -n ci-cd delete secret kaniko-push-secret || true;
kubectl -n ci-cd create secret generic kaniko-push-secret --from-file=config.json=secrets/config.json;
kubectl -n ci-cd label secret kaniko-push-secret skaffold-kaniko=skaffold-kaniko;
```

- Copy the [secrets/skaffold.example.env](/secrets/skaffold.example.env) file to **secrets/skaffold.env**

- Edit the **secrets/skaffold.env** file so that it matches your environment

- After editing it accordingly, source the **secrets/skaffold.env** file so that you can use your own parameters in the subsequent commands

```bash
source secrets/skaffold.env
```

- Create a namespace in your claster, where the application can be deployed by skaffold, if needed (for our example we will use **sample-app** as our namespace). For more details please see <https://skaffold.dev/docs/references/cli/#skaffold-deploy>

```bash
kubectl create namespace sample-app
```

- Create a pull secret in the previously created namespace, so that Kubernetes can pull the images built by **Skaffold**

```bash
kubectl -n sample-app delete secret skaffold-pull-secret|| true;
kubectl -n sample-app create secret docker-registry \
        --docker-server="${DOCKER_REGISTRY_SERVER}" \
        --docker-username="${DOCKER_REGISTRY_USERNAME}" \
        --docker-password="${DOCKER_REGISTRY_PASSWORD}" \
        skaffold-pull-secret
```

- Associate the previously created secret with the **default** service account in the created namespace

```bash
kubectl -n sample-app patch serviceaccount default -p '{"imagePullSecrets": [{"name": "skaffold-pull-secret"}]}'
```

- Configure **Skaffold** to use the desired repository prefix (in our case **docker.io/apsro07/sample-app**) running the following commands

```bash
skaffold config set default-repo docker.io/apsro07
skaffold config set --global collect-metrics false
```

## Building your images using Skaffold

- Build the Docker images for the application
  **NOTE** Please replace \<your-tag\> with the actual tag

```bash
skaffold build --profile ci-cd --tag=<your-tag>
```

## Deploy your application using Skaffold

- Deploy the Kubernetes manifests on top of your cluster
  **NOTE** Please replace \<your-tag\> with the actual tag

```bash
skaffold deploy --profile ci-cd --images sample-app:<your-tag>
```

## Run your image in "live reload" mode

- Deploy the Kubernetes manifests on top of your cluster
  **NOTE** Please replace \<your-tag\> with the actual tag

```bash
skaffold run --profile ci-cd --images sample-app:<your-tag>
```
